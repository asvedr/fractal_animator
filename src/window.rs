use crate::utils::between;


pub struct WindowFactory {
    params_begin: (f64, f64, f64, f64),
    params_end: (f64, f64, f64, f64),
}


impl WindowFactory {

    pub fn new(
        params_begin: (f64, f64, f64, f64),
        params_end: (f64, f64, f64, f64),
    ) -> WindowFactory {
        WindowFactory {
            params_begin: params_begin,
            params_end: params_end,
        }
    }

    pub fn produce(&self, percent: f64) -> (f64, f64, f64, f64) {
        let (x_from_0, y_from_0, x_to_0, y_to_0) = self.params_begin;
        let (x_from_1, y_from_1, x_to_1, y_to_1) = self.params_end;
        (
            between(x_from_0, x_from_1, percent),
            between(y_from_0, y_from_1, percent),
            between(x_to_0, x_to_1, percent),
            between(y_to_0, y_to_1, percent),
        )
    }
}