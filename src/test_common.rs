#[macro_export]
macro_rules! assert_eq_float {
    ($a:expr, $b:expr, $accuracy:expr) => {
        if ($a - $b).abs() > $accuracy {panic!("{} != {}", $a, $b)}
    };
    ($a:expr, $b:expr) => (assert_eq_float!($a, $b, 0.001));
}