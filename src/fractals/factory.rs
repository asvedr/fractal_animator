use crate::utils::between;
use crate::fractals::fractal::Fractal;
use crate::fractals::mandelbort::Mandelbort;


fn make_constructor(method: &str) -> Box<Fn(f64, f64, usize) -> Box<dyn Fractal>> {
    match method {
        "mandelbort" => Box::new(|x, y, d|{Box::new(Mandelbort::new(x, y, d))}),
        _ => panic!("invalid fractal type {}", method),
    }
}


pub struct FractalFactory {
    params_begin: (f64, f64, f64),
    params_end: (f64, f64, f64),
    constructor: Box<Fn(f64, f64, usize) -> Box<dyn Fractal>>,

    input: (String, (f64, f64, usize), (f64, f64, usize)),
}


impl FractalFactory {

    pub fn new(
        method: &str,
        params_begin: (f64, f64, usize),
        params_end: (f64, f64, usize),
    ) -> FractalFactory {

        fn prepare(value: (f64, f64, usize)) -> (f64, f64, f64) {
            let (a, b, c) = value;
            (a, b, c as f64)
        }

        let input = (method.to_string(), params_begin.clone(), params_end.clone());
        FractalFactory {
            params_begin: prepare(params_begin),
            params_end: prepare(params_end),
            constructor: make_constructor(method),
            input: input,
        }
    }

    pub fn produce(&self, percent: f64) -> Box<dyn Fractal> {
        let (bx0, by0, bdepth) = self.params_begin;
        let (ex0, ey0, edepth) = self.params_end;
        let x0 = between(bx0, ex0, percent);
        let y0 = between(by0, ey0, percent);
        let depth = between(bdepth, edepth, percent) as usize;
        (self.constructor)(x0, y0, depth)
    }
}


impl Clone for FractalFactory {
    fn clone(&self) -> FractalFactory {
        let (ref m, ref b, ref e) = self.input;
        FractalFactory::new(m, b.clone(), e.clone())
    }
}
