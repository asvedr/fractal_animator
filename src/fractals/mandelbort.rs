use crate::fractals::fractal::Fractal;


pub struct Mandelbort {
    x0_sq: f64,
    y0_sq: f64,
    x0y0_2: f64,
    depth: usize,
    f_depth: f64,
}


impl Mandelbort {
    pub fn new(x0: f64, y0: f64, depth: usize) -> Mandelbort {
        Mandelbort {
            x0_sq: x0 * x0,
            y0_sq: y0 * y0,
            x0y0_2: x0 * y0 * 2.0,
            depth: depth,
            f_depth: depth as f64,
        }
    }
}


impl Fractal for Mandelbort {
    fn get_fractal_point(&self, x: f64, y: f64) -> f64 {
        let mut zx_sq = self.x0_sq;
        let mut zy_sq = self.y0_sq;
        let mut zxy_2 = self.x0y0_2;
        for i in 0 .. self.depth {
            let zx1 = zx_sq - zy_sq + x;
            let zy1 = zxy_2 + y;
            zx_sq = zx1 * zx1;
            zy_sq = zy1 * zy1;
            zxy_2 = zx1 * zy1 * 2.0;
            if zx_sq + zy_sq > 4.0 {
                return i as f64 / self.f_depth as f64;
            }
        }
        1.0
    }
}
