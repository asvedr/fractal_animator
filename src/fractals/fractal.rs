pub trait Fractal {
    fn get_fractal_point(&self, x: f64, y: f64) -> f64;
}
