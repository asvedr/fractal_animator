use serde::{Serialize, Deserialize};
use serde_json;
use std::io::{self, Read};


#[derive(Debug, Serialize, Deserialize)]
pub struct Fractal {
    pub method: String,
    pub source: (f64, f64, usize),
    pub destination: (f64, f64, usize),
}


#[derive(Debug, Serialize, Deserialize)]
pub struct Color {
    pub method: String,
    pub source: Vec<[u8; 3]>,
    pub destination: Vec<[u8; 3]>,
}


#[derive(Debug, Serialize, Deserialize)]
pub struct Intence {
    pub method: String,
    pub source: Vec<(f64, f64)>,
    pub destination: Vec<(f64, f64)>,
}


#[derive(Debug, Serialize, Deserialize)]
pub struct Args {
    pub fractal: Fractal,
    pub intence: Intence,
    pub color: Color,
    pub frame_count: usize,
    pub dimensions: (u16, u16),
    pub output: String,
    pub analyzer_window: Option<usize>,
}


impl Args {
    pub fn from_stdin() -> io::Result<Args> {
        let mut buffer = String::new();
        let stdin = io::stdin();
        let mut handle = stdin.lock();
        handle.read_to_string(&mut buffer)?;
        serde_json::from_str(&buffer).map_err(
            |err|{
                println!("{:?}", err);
                io::Error::new(io::ErrorKind::Other, "can not parse json")
            }
        )
    }
}
