use crate::analyzer::stat::Stat;

pub type Data = Vec<Vec<f64>>;
pub type CheckFunc = Box::<Fn(&Data) -> CheckResult>;


#[derive(PartialEq, Eq, Debug, Clone, Copy, PartialOrd, Ord)]
pub enum CheckResult {
    Success,
    Unknown,
    Fail,
}


impl CheckResult {

    pub fn ok_for_next_level(&self) -> bool {
        *self != CheckResult::Fail
    }

    pub fn ok_for_result(&self) -> bool {
        *self == CheckResult::Success
    }

    pub fn join(results: Vec<CheckResult>) -> CheckResult {
        if results.len() == 0 {
            return CheckResult::Unknown
        }
        let mut total = CheckResult::Success;
        for val in results {
            if val > total {
                total = val;
            }
        }
        total
    }
}


pub trait Euristic {
    fn check(&self, stat: &Stat) -> CheckResult;
    fn build_stat(&self, width: usize, height: usize) -> Stat;
    fn get_target_scale(&self) -> (f64, f64);

    fn check_bool(&self, stat: &Stat) -> bool {
        self.check(stat) == CheckResult::Success
    }
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_join() {
        assert_eq!(
            CheckResult::join(vec![CheckResult::Success, CheckResult::Fail, CheckResult::Unknown]),
            CheckResult::Fail
        );
        assert_eq!(
            CheckResult::join(vec![CheckResult::Success, CheckResult::Unknown]),
            CheckResult::Unknown
        );
        assert_eq!(
            CheckResult::join(vec![CheckResult::Success]),
            CheckResult::Success
        );
        assert_eq!(CheckResult::join(vec![]), CheckResult::Unknown);
    }
}
