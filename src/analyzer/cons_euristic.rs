use crate::analyzer::stat::Stat;
use crate::analyzer::base_euristic::{Data, CheckFunc, CheckResult, Euristic};


pub struct ConsEuristic {
    funs: Vec<CheckFunc>,
    intence_map: Vec<f64>,
    target_scale: (f64, f64),
}


impl ConsEuristic {

    pub fn new(
        target_scale: (f64, f64),
        intence_map: Vec<f64>,
        funs: Vec<CheckFunc>,
    ) -> ConsEuristic {
        ConsEuristic {
            funs: funs,
            intence_map: intence_map,
            target_scale: target_scale,
        }
    }

}


impl Euristic for ConsEuristic {

    fn check(&self, stat: &Stat) -> CheckResult {
        let data = stat.results();
        for fun in self.funs.iter() {
            let result = fun(&data);
            if result != CheckResult::Unknown {
                return result;
            }
        }
        CheckResult::Unknown
    }

    fn build_stat(&self, width: usize, height: usize) -> Stat {
        Stat::new(width, height, self.intence_map.clone())
    }

    fn get_target_scale(&self) -> (f64, f64) {
        self.target_scale.clone()
    }
}


#[cfg(test)]
mod tests {

    use super::*;

    fn exclude_2(data: &Data) -> CheckResult {
        if data[0][2] > 0.2 {CheckResult::Fail} else {CheckResult::Unknown}
    }

    fn apply_3(data: &Data) -> CheckResult {
        if data[0][3] > 0.2 {CheckResult::Success} else {CheckResult::Unknown}
    }

    fn make_cons_euristics() -> ConsEuristic {
        ConsEuristic::new(
            (0.0, 0.007),
            vec![0.0, 0.2, 0.4, 0.6, 0.8],
            vec![Box::new(&exclude_2), Box::new(&apply_3)],
        )
    }

    #[test]
    fn test_cons() {
        let cons_euristics = make_cons_euristics();

        let neutral_stat = cons_euristics.build_stat(3, 3);
        assert_eq!(cons_euristics.check(&neutral_stat), CheckResult::Unknown);

        let mut ok_stat = cons_euristics.build_stat(3, 3);
        for _ in 0 .. 5 {
            ok_stat.add_pixel(0, 0, 0.7)
        }
        assert_eq!(cons_euristics.check(&ok_stat), CheckResult::Success);

        let mut fail_stat = cons_euristics.build_stat(3, 3);
        for _ in 0 .. 5 {
            fail_stat.add_pixel(0, 0, 0.7);
            fail_stat.add_pixel(1, 1, 0.5);
        }
        assert_eq!(cons_euristics.check(&fail_stat), CheckResult::Fail);
    }

}
