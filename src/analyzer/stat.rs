const PART_COUNT: usize = 3;


#[derive(Debug)]
struct PixelStat {
    intences: Vec<f64>
}


#[derive(Debug)]
pub struct Stat {
    width: usize,
    height: usize,
    part_width: usize,
    part_height: usize,
    total: PixelStat,
    parts: Vec<PixelStat>,
    intence_map: Vec<f64>,
}


/*
    0|1|2
    -----
    3|4|5
    -----
    6|7|8
*/


unsafe impl Sync for Stat {}
unsafe impl Send for Stat {}


impl PixelStat {
    fn new(count: usize) -> PixelStat {
        PixelStat {
            intences: (0 .. count).map(|_|{0.0}).collect()
        }
    }
    fn inc_index(&mut self, index: usize) {
        self.intences[index] += 1.0;
    }
    fn extend(&mut self, other: &PixelStat) {
        for i in 0 .. self.intences.len() {
            self.intences[i] += other.intences[i];
        }
    }
    fn percentage(&self, total_pixel_count: f64) -> Vec<f64> {
        self.intences.iter()
        .map(|count|{count / total_pixel_count})
        .collect::<Vec<f64>>()
    }
}


impl Stat {

    pub fn new(width: usize, height: usize, intence_map: Vec<f64>) -> Stat {
        let mut part_width = width / PART_COUNT;
        if part_width * PART_COUNT < width {
            part_width += 1;
        }
        let mut part_height = height / PART_COUNT;
        if part_height * PART_COUNT < height {
            part_height += 1;
        }
        let map_len = intence_map.len();
        Stat {
            width: width,
            height: height,
            part_width: part_width,
            part_height: part_height,
            total: PixelStat::new(map_len),
            parts: (0 .. PART_COUNT * PART_COUNT)
                        .map(|_|{PixelStat::new(map_len)})
                        .collect(),
            intence_map: intence_map,
        }        
    }

    fn get_part_index(&self, x: usize, y: usize) -> usize {
        let part_x = x / self.part_width;
        let part_y = y / self.part_height;
        part_y * PART_COUNT + part_x
    }

    fn get_intence_index(&self, intence: f64) -> usize {
        let mut left = 0;
        let mut right = self.intence_map.len();
        while right > left {
            let index = (left + right) / 2;
            if intence >= self.intence_map[index] {
                left = index + 1;
            } else {
                right = index;
            }
        }
        left - 1
    }

    pub fn add_pixel(&mut self, x: usize, y: usize, intence: f64) {
        let intence_index = self.get_intence_index(intence);
        self.total.inc_index(intence_index);
        let index = self.get_part_index(x, y);
        self.parts[index].inc_index(intence_index);
    }

    pub fn extend(&mut self, other: &Stat) {
        self.total.extend(&other.total);
        for i in 0 .. self.parts.len() {
            self.parts[i].extend(&other.parts[i]);
        }
    }

    pub fn results(&self) -> Vec<Vec<f64>> {
        let total_pixel_count = (self.width * self.height) as f64;
        let mut result = vec![self.total.percentage(total_pixel_count)];
        let part_pixel_count = (self.part_width * self.part_height) as f64;
        for part in self.parts.iter() {
            result.push(part.percentage(part_pixel_count));
        }
        result
    }

    pub fn show(&self) -> Vec<(&'static str, String)> {
        let headers = vec![
            "qtotal",
            "q0", "q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8",
        ];
        headers.iter().zip(self.results().iter())
            .map(|(name, pair)|{(*name, format!("{:?}", pair))})
            .collect::<Vec<(&'static str, String)>>()
    }

}


#[cfg(test)]
mod tests {

    use super::*;

    macro_rules! assert_eq_vec {
        ($a:expr, $b:expr) => {{
            if $a.len() != $b.len() {
                panic!("{:?} != {:?}", $a, $b);                
            }
            for i in 0 .. $a.len() {
                let pa = $a[i];
                let pb = $b[i];
                if !((pa - pb).abs() < 0.001) {
                    panic!("{:?} != {:?}", $a, $b);
                }
            }
        }}
    }

    #[test]
    fn test_index() {
        let stat = Stat::new(10, 10, vec![0.0, 0.2, 0.4, 0.8]);
        assert_eq!(stat.get_intence_index(0.1), 0);
        assert_eq!(stat.get_intence_index(0.3), 1);
        assert_eq!(stat.get_intence_index(0.6), 2);
        assert_eq!(stat.get_intence_index(0.9), 3);
    }

    #[test]
    fn test_percentage() {
        let mut stat = PixelStat::new(3);
        assert_eq_vec!(stat.percentage(10.0), vec![0.0, 0.0, 0.0]);
        for _ in 0 .. 5 {
            stat.inc_index(1);
        }
        assert_eq_vec!(stat.percentage(10.0), vec![0.0, 0.5, 0.0]);
        for _ in 0 .. 2 {
            stat.inc_index(0);
            stat.inc_index(2);
        }
        assert_eq_vec!(stat.percentage(10.0), vec![0.2, 0.5, 0.2]);
    }

    #[test]
    fn test_extend() {
        let mut stat = PixelStat {
            intences: vec![5.0, 3.0],
        };
        stat.extend(&PixelStat{intences: vec![5.0, 4.0]});
        assert_eq_vec!(stat.percentage(10.0), vec![1.0, 0.7]);
    }

    #[test]
    fn test_stat_part_choice() {
        let mut stat = Stat::new(9, 9, vec![-0.1, 0.5]);
        let len = 81.0;
        let part_len = 9.0;
        let of_light = vec![
            (0, 0), (0, 2), (1, 1),
            (0, 8)
        ];
        let of_dark = vec![
            (4, 4), (4, 0), (7, 7),
        ];
        for (x, y) in of_light.iter() {
            stat.add_pixel(*x, *y, 1.0);
        }
        for (x, y) in of_dark.iter() {
            stat.add_pixel(*x, *y, 0.0);
        }
        let results = stat.results();
        // total
        assert_eq_vec!(
            results[0],
            vec![of_dark.len() as f64 / len, of_light.len() as f64 / len]
        );
        // 0
        assert_eq_vec!(results[1], vec![0.0, 3.0 / part_len]);
        // 1
        assert_eq_vec!(results[2], vec![1.0 / part_len, 0.0]);
        // 2
        assert_eq_vec!(results[3], vec![0.0, 0.0]);
        // 3
        assert_eq_vec!(results[4], vec![0.0, 0.0]);
        // 4
        assert_eq_vec!(results[5], vec![1.0 / part_len, 0.0]);
        // 5
        assert_eq_vec!(results[6], vec![0.0, 0.0]);
        // 6
        assert_eq_vec!(results[7], vec![0.0, 1.0 / part_len]);
        // 7
        assert_eq_vec!(results[8], vec![0.0, 0.0]);
        // 8
        assert_eq_vec!(results[9], vec![1.0 / part_len, 0.0]);
    }

    #[test]
    fn test_get_part_index() {
        let mut stat = Stat::new(100, 100, vec![0.0, 0.5]);
        assert_eq!(stat.get_part_index(0, 99), 6);
    }
}
