mod stat;
mod base_euristic;
mod cons_euristic;
mod black_and_white_euristic;
mod builder;

pub use stat::Stat;
pub use base_euristic::{Euristic, CheckResult};
pub use builder::EuristicType;
