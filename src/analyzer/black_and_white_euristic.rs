use crate::analyzer::base_euristic::{Data, CheckResult};
use crate::analyzer::cons_euristic::ConsEuristic;


fn hyp_exclude_too_dark_and_too_light(data: &Data) -> CheckResult {
    let mut all_quad_are_dark = true;
    let mut all_quad_are_light = true;
    for quad in &data[1 .. ] {
        if quad[4] > 0.03 {
            all_quad_are_dark = false;
        } else if quad[4] < 0.5 {
            all_quad_are_light = false;
        }
    }
    if all_quad_are_dark || all_quad_are_light || data[0][4] >= 0.55 {
        CheckResult::Fail
    } else {
        CheckResult::Unknown
    }
}


fn exclude_one_color(data: &Data) -> CheckResult {
    for item in data[0].iter() {
        if *item > 0.89 {
            return CheckResult::Fail
        }
    }
    CheckResult::Unknown
}


fn hyp_3_of_half_dark(data: &Data) -> CheckResult {
    let mut half_dark_count = 0;
    for quad in &data[1 .. ] {
        if quad[1] > 0.4 {
            half_dark_count += 1;
        }
    }
    if data[0][0] > 0.4 && half_dark_count >= 3 {
        CheckResult::Success
    } else {
        CheckResult::Unknown
    }
}


fn hyp_total_half_dark(data: &Data) -> CheckResult {
    if data[0][1] > 0.3 && data[0][2] > 0.1 {
        return CheckResult::Success
    } else {
        return CheckResult::Unknown
    }
}


pub fn make() -> ConsEuristic {
    ConsEuristic::new(
        (0.0, 0.007),
        vec![0.0, 0.2, 0.4, 0.6, 0.8],
        vec![
            Box::new(&hyp_exclude_too_dark_and_too_light),
            Box::new(&exclude_one_color),
            Box::new(&hyp_3_of_half_dark),
            Box::new(&hyp_total_half_dark),
        ],
    )
}


#[cfg(test)]
mod tests {

    use super::*;
    use crate::fractals::{Mandelbort, Fractal};
    use crate::analyzer::stat::Stat;
    use crate::analyzer::base_euristic::Euristic;

    const SIZE: usize = 100;

    fn render_window(analyzer: &ConsEuristic, window: (f64, f64, f64, f64)) -> Stat {
        let (x0, y0, x1, y1) = window;
        let dx = x1 - x0;
        let dy = y1 - y0;
        let size_f64 = SIZE as f64;
        let mut stat = analyzer.build_stat(SIZE, SIZE);
        let fractal = Mandelbort::new(0.0, 0.0, 170);
        for x in 0 .. SIZE {
            let window_x = x0 + ((x as f64 / size_f64) * dx);
            for y in 0 .. SIZE {
                let window_y = y0 + ((y as f64 / size_f64) * dy);
                let intense = fractal.get_fractal_point(window_x, window_y);
                stat.add_pixel(x, y, intense);
            }
        }
        stat
    }

    #[test]
    fn test_too_light() {
        let window = (0.3800945933908224, 0.2761558317579329, 0.3800945966504514, 0.2761558350175619);
        let euristic = make();
        let mut stat = render_window(&euristic, window);
        let results = stat.results();
        println!("{:?}", results);
        assert!(results[0][4] < 0.9);
    }
}
