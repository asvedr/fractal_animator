use crate::analyzer::base_euristic::Euristic;
use crate::analyzer::black_and_white_euristic;


pub enum EuristicType {
    BlackAndWhite,
    Other,
}


impl EuristicType {
    pub fn build(&self) -> Box<dyn Euristic> {
        match self {
            EuristicType::BlackAndWhite => Box::new(black_and_white_euristic::make()),
            _ => panic!("not implemented"),
        }
    }
}
