#[macro_use]
extern crate serde;
extern crate rand;

#[cfg(test)]
#[macro_use]
mod test_common;
mod utils;
mod color;
mod window;
mod gradient;
mod fractals;
mod render;
mod analyzer;
mod finder;
mod cli;

use std::io;

use crate::gradient::{ColorFactory, IntenceFactory};
use crate::fractals::FractalFactory;
use crate::render::RenderGif;
use crate::window::WindowFactory;
use crate::cli::Args;
use crate::analyzer::EuristicType;
use crate::finder::Finder;


// INTENCES
// plain: "linear" [[0,0], [1,1]]
// with overlight: "linear" [[0,0], [0.4, 0.4], [0.8, 1], [0.9, 0.75], [1,0]],
// stairs: "linear": [[0,0], [0.2, 1], [0.4, 0], [0.6, 1], [0.8, 0], [1,1]]


fn main() -> io::Result<()> {
    let args = Args::from_stdin()?;
    let fractal = FractalFactory::new(
        &args.fractal.method.clone(),
        args.fractal.source.clone(),
        args.fractal.destination.clone(),
    );
    let intence = IntenceFactory::new(
        &args.intence.method.clone(),
        args.intence.source.clone(),
        args.intence.destination.clone(),
    );
    let color = ColorFactory::new(
        &args.color.method.clone(),
        &args.color.source.clone(),
        &args.color.destination.clone(),
    );
    let mut finder = Finder::new(EuristicType::BlackAndWhite, fractal.clone());
    if let Some(size) = args.analyzer_window {
        finder.stat_size = size;
    }
    let window = match finder.find_window() {
        Some(value) => value,
        None => panic!("window not found"),
    };
    // let window = (-2.0, -2.0, 2.0, 2.0); //window.clone();
    let window_factory = WindowFactory::new(window.clone(), window.clone());
    let render = RenderGif::new(fractal, intence, color, window_factory);
    render.render(
        args.dimensions.clone(),
        args.frame_count.clone(),
        true,
        &format!("{}.gif", args.output),
    )?;
    Ok(())
}
