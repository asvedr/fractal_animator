#[inline(always)]
pub fn between(a: f64, b: f64, percent: f64) -> f64 {
    a + (b - a) * percent
}


#[inline(always)]
pub fn between_point(a: (f64, f64), b: (f64, f64), percent: f64) -> (f64, f64) {
    let (ax, ay) = a;
    let (bx, by) = b;
    (between(ax, ay, percent), between(bx, by, percent))
}
