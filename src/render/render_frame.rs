use image::{ImageBuffer, Rgb};

use crate::fractals::Fractal;
use crate::gradient::{GradientIntence, GradientColor};


pub struct RenderFrame {
    fractal_generator: Box<dyn Fractal>,
    gradient_intence: Box<dyn GradientIntence>,
    gradient_color: Box<dyn GradientColor>,
}


impl RenderFrame {

    pub fn new(
        fractal: Box<dyn Fractal>,
        intence: Box<dyn GradientIntence>,
        color: Box<dyn GradientColor>,
    ) -> RenderFrame {
        RenderFrame {
            fractal_generator: fractal,
            gradient_intence: intence,
            gradient_color: color,
        }
    }

    pub fn render_image(
        &mut self,
        window: (f64, f64, f64, f64),
        dimension: (u32, u32),
    ) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
        let (x_from, y_from, x_to, y_to) = window;
        let (width, height) = dimension;
        let mut buffer = ImageBuffer::new(width, height);
        let x_vec = x_to - x_from;
        let y_vec = y_to - y_from;
        for cy in 0 .. height {
            for cx in 0 .. width {
                let x = x_from + x_vec * (cx as f64 / width as f64);
                let y = y_from + y_vec * (cy as f64 / height as f64);
                let intence = self.fractal_generator.get_fractal_point(x, y);
                let intence = self.gradient_intence.get_intence(intence);
                let color = self.gradient_color.get_color(intence);
                buffer.put_pixel(cx, cy, Rgb(color));
            }
        }
        buffer
    }
}
