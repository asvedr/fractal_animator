use std::io;
use std::fs::File;
use std::thread;

use gif::{Encoder, Frame, Repeat, SetParameter};

use crate::fractals::{Fractal, FractalFactory};
use crate::gradient::{GradientIntence, GradientColor, ColorFactory, IntenceFactory};
use crate::window::WindowFactory;


pub struct RenderGif {
    fractal_factory: FractalFactory,
    intence_factory: IntenceFactory,
    color_factory: ColorFactory,
    window_factory: WindowFactory,
}


struct ThreadData {
    fractal: Box<dyn Fractal>,
    intence: Box<dyn GradientIntence>,
    color: Box<dyn GradientColor>,
    window: (f64, f64, f64, f64),
    dimension: (u16, u16),
}


unsafe impl Send for ThreadData {}
unsafe impl Sync for ThreadData {}


fn render_frame(params: ThreadData) -> Frame<'static> {

    let fractal_generator = params.fractal;
    let mut gradient_intence = params.intence;
    let mut gradient_color = params.color;
    let window = params.window;
    let dimension = params.dimension;

    let (x_from, y_from, x_to, y_to) = window;
    let (width, height) = dimension;
    let x_vec = x_to - x_from;
    let y_vec = y_to - y_from;
    let mut bytes = vec![];
    bytes.reserve((width as usize) * (height as usize) * 3);
    for cy in 0 .. height {
        for cx in 0 .. width {
            let x = x_from + x_vec * (cx as f64 / width as f64);
            let y = y_from + y_vec * (cy as f64 / height as f64);
            let intence = fractal_generator.get_fractal_point(x, y);
            let intence = gradient_intence.get_intence(intence);
            let color = gradient_color.get_color(intence);
            bytes.extend(color.iter());
            // buffer.put_pixel(cx, cy, Rgb(color));
        }
    }
    Frame::from_rgb(width, height, &bytes)
}



impl RenderGif {

    pub fn new(
        fractal_factory: FractalFactory,
        intence_factory: IntenceFactory,
        color_factory: ColorFactory,
        window_factory: WindowFactory,
    ) -> RenderGif {
        RenderGif {
            fractal_factory: fractal_factory,
            intence_factory: intence_factory,
            color_factory: color_factory,
            window_factory: window_factory,
        }
    }

    #[inline(always)]
    fn make_thread_data(&self, dimension: (u16, u16), percent: f64) -> ThreadData {
        ThreadData {
            fractal: self.fractal_factory.produce(percent),
            intence: self.intence_factory.produce(percent),
            color: self.color_factory.produce(percent),
            window: self.window_factory.produce(percent),
            dimension: dimension,
        }
    }

    pub fn render(
        &self,
        dimension: (u16, u16),
        frame_count: usize,
        add_reverse: bool,
        output: &str
    ) -> io::Result<()> {
        let mut outfile = File::create(output)?;
        let (width, height) = dimension.clone();
        let mut encoder = Encoder::new(&mut outfile, width, height, &[])?;
        encoder.set(Repeat::Infinite)?;

        let f_count = frame_count as f64;
        let mut used_frames = vec![];
        let mut handlers = vec![];
        for frame_index in 0 .. (frame_count + 1) {
            let percent = frame_index as f64 / f_count;
            let thread_data = self.make_thread_data(dimension, percent);
            let handler = thread::spawn(move||{render_frame(thread_data)});
            handlers.push(handler);
        }
        for (i, handle) in handlers.into_iter().enumerate() {
            let frame = handle.join().expect("failed frame");
            println!("front {} of {}", i, frame_count);
            encoder.write_frame(&frame)?;
            if add_reverse {
                used_frames.push(frame);
            }
        }
        for (i, frame) in used_frames.iter().rev().enumerate() {
            println!("back {} of {}", i, frame_count);
            encoder.write_frame(frame)?;
        }
        Ok(())
    }

}
