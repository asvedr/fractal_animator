pub type Color = [u8; 3];
pub type FColor = [f64; 3];


pub fn black() -> Color {
    [0, 0, 0]
}


pub fn color2fcolor(color: &Color) -> FColor {
    [
        color[0] as f64,
        color[1] as f64,
        color[2] as f64,
    ]
}


#[inline(always)]
pub fn fcolor2color(fcolor: &FColor) -> Color {
    [
        fcolor[0] as u8,
        fcolor[1] as u8,
        fcolor[2] as u8,
    ]    
}


#[inline(always)]
pub fn between_fcolor(a: &FColor, b: &FColor, percent: f64) -> FColor {
    let mut color = [0.0; 3];
    for i in 0 .. 3 {
        color[i] = a[i] + (b[i] - a[i]) * percent;
    }
    color
}
