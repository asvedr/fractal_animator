use rand::prelude::random;

use crate::analyzer::{Stat, EuristicType, CheckResult, Euristic};
use crate::fractals::{Fractal, FractalFactory};


type Window = (f64, f64, f64, f64);


const DEFAULT_INIT_WINDOW: Window = (-2.0, -1.75, 1.5, 1.75);
const DEFAULT_FINDER_STEP: usize = 8;
const DEFAULT_STAT_SIZE: usize = 100;
const DEFAULT_PERCENTAGES_TO_CHECK: &'static [f64] = &[0.0, 1.0];


pub struct Finder {
    analyzer: Box<dyn Euristic>,
    fractal_factory: FractalFactory,
    pub init_window: Window,
    pub finder_step: usize,
    pub stat_size: usize,
    pub percentages_to_check: Vec<f64>,
}


fn shuffle_vec<T: Clone>(mut array: Vec<T>) -> Vec<T> {
    let range = array.len();
    let mut visibility_map: Vec<bool> = (0 .. range).map(|_|{true}).collect();
    let mut result = Vec::new();
    result.reserve(range);
    for _ in 0 .. range {
        let mut i = random::<usize>() % range;
        while !visibility_map[i] {
            i = (i + 1) % range;
        }
        result.push(array[i].clone());
        visibility_map[i] = false;
    }
    result
}


impl Finder {

    pub fn new(euristic_type: EuristicType, fractal_factory: FractalFactory) -> Finder {
        Finder {
            analyzer: euristic_type.build(),
            fractal_factory: fractal_factory,
            init_window: DEFAULT_INIT_WINDOW.clone(),
            finder_step: DEFAULT_FINDER_STEP,
            stat_size: DEFAULT_STAT_SIZE,
            percentages_to_check: DEFAULT_PERCENTAGES_TO_CHECK.iter().cloned().collect(),
        }
    }

    fn render_windows(&self, fractals: &[Box<dyn Fractal>], window: Window) -> Vec<Stat> {
        let (x0, y0, x1, y1) = window;
        let dx = x1 - x0;
        let dy = y1 - y0;
        let size = self.stat_size;
        let size_f64 = size as f64;
        let mut stats = vec![];
        for _ in 0 .. fractals.len() {
            stats.push(self.analyzer.build_stat(size, size));
        }
        for x in 0 .. size {
            let window_x = x0 + ((x as f64 / size_f64) * dx);
            for y in 0 .. size {
                let window_y = y0 + ((y as f64 / size_f64) * dy);
                for i in 0 .. fractals.len() {
                    let intense = fractals[i].get_fractal_point(window_x, window_y);
                    stats[i].add_pixel(x, y, intense);
                }
            }
        }
        stats
    }

    fn is_scale_in_target(&self, window: &Window) -> bool {
        let (x, _, dx, _) = window;
        let (min_scale, max_scale) = self.analyzer.get_target_scale();
        let scale = (*dx - *x).abs();
        scale > min_scale && scale < max_scale
    }

    fn split_for_next_level(&self, window: Window) -> Vec<Window> {
        let (x0, y0, x1, y1) = window;
        let step = self.finder_step;
        let divider = step as f64;
        let vec_x = x1 - x0;
        let vec_y = y1 - y0;
        let mut result = Vec::new();
        for x in 0 .. step {
            let new_x0 = x0 + (x as f64 / divider) * vec_x;
            let new_x1 = x0 + ((x + 1) as f64 / divider) * vec_x;
            for y in 0 .. step {
                let new_y0 = y0 + (y as f64 / divider) * vec_y;
                let new_y1 = y0 + ((y + 1) as f64 / divider) * vec_y;
                result.push((new_x0, new_y0, new_x1, new_y1));
            }
        }
        let result = shuffle_vec(result);
        shuffle_vec(result)
    }

    pub fn find_window(&self) -> Option<Window> {
        let mut stack = vec![(self.init_window.clone(), 0)];
        let fractals: Vec<Box<dyn Fractal>> = self.percentages_to_check
            .iter()
            .map(|percent|{self.fractal_factory.produce(*percent)})
            .collect();
        loop {
            let (window, depth) = stack.pop()?;
            let stats = self.render_windows(&fractals, window);
            let results: Vec<CheckResult> = stats.iter()
                .map(|stat|{self.analyzer.check(stat)})
                .collect();
            let result = CheckResult::join(results);
            if result.ok_for_result() && self.is_scale_in_target(&window) {
                return Some(window);
            }
            if result.ok_for_next_level() {
                let layer = self.split_for_next_level(window);
                let layer: Vec<(Window, usize)> = layer.iter()
                    .cloned()
                    .map(|window|{(window, depth+1)}).collect();
                stack.extend(&layer);
            }
        }
        None
    }
}


#[cfg(test)]
mod tests {

    use super::*;
    use std::collections::HashSet;
    use crate::fractals::Mandelbort;

    fn window_eq(a: &Window, b: &Window) -> bool {
        let (ax0, ay0, adx, ady) = a;
        let (bx0, by0, bdx, bdy) = b;
        for (i, j) in [(ax0, bx0), (ay0, by0), (adx, bdx), (ady, bdy)].iter() {
            if (*i - *j).abs() > 0.0001 {
                return false;
            }
        }
        true
    }

    fn mandelbort() -> FractalFactory {
        FractalFactory::new("mandelbort", (0.0, 0.0, 100), (0.0, 0.0, 100))
    }

    #[test]
    fn test_is_scale_in_target() {
        let finder = Finder::new(EuristicType::BlackAndWhite, mandelbort());
        assert!(!finder.is_scale_in_target(&(0.0, 0.0, 1.0, 1.0)));
        assert!(finder.is_scale_in_target(&(0.0, 0.0, 0.001, 0.001)));
    }

    #[test]
    fn test_split_for_next_level() {
        let mut finder = Finder::new(EuristicType::BlackAndWhite, mandelbort());
        finder.finder_step = 4;
        let results = finder.split_for_next_level((0.0, 0.0, 1.0, 1.0));
        let ethalon = vec![
            (0.00, 0.0, 0.25, 0.25),
            (0.00, 0.25, 0.25, 0.50),
            (0.00, 0.5, 0.25, 0.75),
            (0.00, 0.75, 0.25, 1.00),

            (0.25, 0.0, 0.50, 0.25),
            (0.25, 0.25, 0.50, 0.50),
            (0.25, 0.5, 0.50, 0.75),
            (0.25, 0.75, 0.50, 1.00),

            (0.50, 0.0, 0.75, 0.25),
            (0.50, 0.25, 0.75, 0.50),
            (0.50, 0.5, 0.75, 0.75),
            (0.50, 0.75, 0.75, 1.00),

            (0.75, 0.0, 1.00, 0.25),
            (0.75, 0.25, 1.00, 0.50),
            (0.75, 0.5, 1.00, 0.75),
            (0.75, 0.75, 1.00, 1.00)
        ];
        assert_eq!(results.len(), ethalon.len());
        for eth in ethalon.iter() {
            let mut matched = false;
            for item in results.iter() {
                if window_eq(eth, item) {
                    matched = true
                }
            }
            assert!(matched, format!("{:?}", *eth));
        }
    }

    #[test]
    fn test_split_for_next_level_2() {
        let mut finder = Finder::new(EuristicType::BlackAndWhite, mandelbort());
        finder.finder_step = 2;
        let results = finder.split_for_next_level((-2.0, -2.0, 2.0, 2.0));
        let ethalon = vec![
            (-2.0, -2.0, 0.0, 0.0),
            (-2.0, 0.0, 0.0, 2.0),
            (0.0, -2.0, 2.0, 0.0),
            (0.0, 0.0, 2.0, 2.0),
        ];
        assert_eq!(results.len(), ethalon.len());
        println!("{:?}", results);
        for eth in ethalon.iter() {
            let mut matched = false;
            for item in results.iter() {
                if window_eq(eth, item) {
                    matched = true
                }
            }
            assert!(matched, format!("{:?}", *eth));
        }
    }

    #[test]
    fn test_shuffle() {
        let mut source: Vec<usize> = (0 .. 100).collect();
        let mut shuffled = shuffle_vec(source.clone());
        source.sort();
        shuffled.sort();
        assert_eq!(source, shuffled);
    }
}
