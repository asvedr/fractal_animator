use crate::gradient::gradients::GradientColor;
use crate::color::{Color, FColor};


#[derive(Debug)]
struct CachedColor {
    color: [f64; 3],
    vec_to_next: [f64; 3],
}


pub struct LinearColor {
    points: Vec<CachedColor>,
    len: f64,
}


impl CachedColor {

    fn new(color: &FColor, next: &FColor) -> CachedColor {
        let mut destination = [0.0; 3];
        for i in 0 .. 3 {
            destination[i] = next[i] - color[i];
        }
        CachedColor {
            color: color.clone(),
            vec_to_next: destination,
        }
    }

    fn get_color(&self, intence: f64) -> Color {
        let mut color = [0; 3];
        for i in 0 .. 3 {
            color[i] = (self.color[i] + self.vec_to_next[i] * intence) as u8;
        }
        color
    }

}


impl LinearColor {
    pub fn new(points: Vec<FColor>) -> LinearColor {
        let len = points.len() as f64;
        let mut cached_points = vec![];
        for i in 0 .. points.len() - 1 {
            cached_points.push(CachedColor::new(&points[i], &points[i + 1]));
        }
        cached_points.push(CachedColor::new(&points[points.len() - 1], &points[points.len() - 1]));
        LinearColor {
            points: cached_points,
            len: len,
        }
    }
}


impl GradientColor for LinearColor {
    fn get_color(&mut self, value: f64) -> Color {
        let scaled = value * self.len;
        let index = scaled as usize;
        let point = &self.points[index];
        point.get_color(scaled - scaled.floor())
    }
}
