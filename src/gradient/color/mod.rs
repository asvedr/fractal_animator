mod bezier;
mod linear;
mod factory;

pub use bezier::BezierColor;
pub use linear::LinearColor;
pub use factory::ColorFactory;
