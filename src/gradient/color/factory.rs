use crate::color::{Color, FColor, color2fcolor, between_fcolor};
use crate::gradient::gradients::GradientColor;
use crate::gradient::color::bezier::BezierColor;
use crate::gradient::color::linear::LinearColor;


fn make_constructor(method: &str) -> Box<Fn(Vec<FColor>) -> Box<dyn GradientColor>> {
    match method {
        "bezier" => Box::new(|args|{Box::new(BezierColor::new(args))}),
        "linear" => Box::new(|args|{Box::new(LinearColor::new(args))}),
        _ => panic!("invalid color method {}", method),
    }
}


pub struct ColorFactory {
    f_params_begin: Vec<FColor>,
    f_params_end: Vec<FColor>,
    constructor: Box<Fn(Vec<FColor>) -> Box<dyn GradientColor>>
}


impl ColorFactory {

    pub fn new(
        method: &str,
        params_begin: &[Color],
        params_end: &[Color],
    ) -> ColorFactory {
        assert_eq!(params_begin.len(), params_end.len());
        ColorFactory {
            f_params_begin: params_begin.iter().map(&color2fcolor).collect(),
            f_params_end: params_end.iter().map(&color2fcolor).collect(),
            constructor: make_constructor(method),
        }
    }

    pub fn produce(&self, percent: f64) -> Box<dyn GradientColor> {
        let mut params = vec![];
        for i in 0 .. self.f_params_begin.len() {
            let param = between_fcolor(&self.f_params_begin[i], &self.f_params_end[i], percent);
            params.push(param);
        }
        (self.constructor)(params)
    }
}