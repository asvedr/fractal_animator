use crate::gradient::gradients::GradientColor;
use crate::color::{Color, FColor, color2fcolor, fcolor2color};


pub struct BezierColor {
    layers: Vec<Vec<FColor>>,
}


impl BezierColor {
    pub fn new(points: Vec<FColor>) -> BezierColor {
        let len = points.len();
        let mut layers = vec![points];
        for i in 1 .. len {
            let layer: Vec<FColor> = (0 .. len - i).map(|_|{[0.0; 3]}).collect();
            layers.push(layer);
        }
        BezierColor {
            layers: layers,
        }
    }
}


#[inline(always)]
fn between(a: FColor, b: FColor, intence: f64, out: &mut FColor) {
    for i in 0 .. a.len() {
        let channel_a = a[i] as f64;
        let channel_b = b[i] as f64;
        out[i] = channel_a + (channel_b - channel_a) * intence;
    }
}


impl GradientColor for BezierColor {
    fn get_color(&mut self, value: f64) -> Color {
        let layers = &mut self.layers;
        let mut buffer = [0.0; 3];
        for layer_index in 0 .. layers.len() - 1 {
            for point_index in 0 .. layers[layer_index].len() - 1 {
                between(
                    layers[layer_index][point_index],
                    layers[layer_index][point_index + 1],
                    value,
                    &mut buffer,
                );
                layers[layer_index + 1][point_index] = buffer.clone();
            }
        }
        fcolor2color(&layers[layers.len() - 1][0])
    }
}
