use crate::color::Color;


pub trait GradientIntence {
    fn get_intence(&mut self, value: f64) -> f64;
}


pub trait GradientColor {
    fn get_color(&mut self, value: f64) -> Color;
}
