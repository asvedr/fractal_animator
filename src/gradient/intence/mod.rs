mod linear;
mod bezier;
mod factory;

pub use linear::LinearIntence;
pub use bezier::BezierIntence;
pub use factory::IntenceFactory;