use crate::gradient::gradients::GradientIntence;


pub struct BezierIntence {
    layers: Vec<Vec<(f64, f64)>>,
    max_x: f64,
}


impl BezierIntence {
    pub fn new(points: Vec<(f64, f64)>) -> BezierIntence {
        let len = points.len();
        let (max_x, _) = points[len - 1];
        let mut layers = vec![points];
        for i in 1 .. len {
            let layer: Vec<(f64, f64)> = (0 .. len - i)
                .map(|_|{(0.0, 0.0)})
                .collect();
            layers.push(layer);
        }
        BezierIntence {
            max_x: max_x,
            layers: layers,
        }
    }
}


#[inline(always)]
fn between(a: (f64, f64), b: (f64, f64), intence: f64) -> (f64, f64) {
    let (ax, ay) = a;
    let (bx, by) = b;
    let x = ax + (bx - ax) * intence;
    let y = ay + (by - ay) * intence;
    (x, y)
}


impl GradientIntence for BezierIntence {
    fn get_intence(&mut self, value: f64) -> f64 {
        let scaled = value * self.max_x;
        let layers = &mut self.layers;
        for layer_index in 0 .. layers.len() - 1 {
            for point_index in 0 .. layers[layer_index].len() - 1 {
                let next_point = between(
                    layers[layer_index][point_index],
                    layers[layer_index][point_index + 1],
                    scaled,
                );
                layers[layer_index + 1][point_index] = next_point;
            }
        }
        let (_, y) = layers[layers.len() - 1][0];
        y
    }
}


#[cfg(test)]
mod tests {

    use super::*;
    use crate::gradient::gradients::GradientIntence;
    use crate::test_common;

    #[test]
    fn test_linear() {
        let mut intence = BezierIntence::new(vec![(0.0, 0.0), (1.0, 1.0)]);
        assert_eq_float!(intence.get_intence(0.0), 0.0);
        assert_eq_float!(intence.get_intence(0.5), 0.5);
        assert_eq_float!(intence.get_intence(0.75), 0.75);
        assert_eq_float!(intence.get_intence(1.0), 1.0);
    }


    #[test]
    fn test_two_points() {
        let mut intence = BezierIntence::new(vec![
            (0.0, 0.0),
            (0.0, 1.0),
            (1.0, 0.0),
            (1.0, 1.0),
        ]);
        assert_eq_float!(intence.get_intence(0.00), 0.0);
        assert_eq_float!(intence.get_intence(0.25), 0.4375);
        assert_eq_float!(intence.get_intence(0.50), 0.5);
        assert_eq_float!(intence.get_intence(0.75), 0.5625);
        assert_eq_float!(intence.get_intence(1.00), 1.0);
    }
}
