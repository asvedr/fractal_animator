use crate::gradient::gradients::GradientIntence;
use crate::gradient::intence::bezier::BezierIntence;
use crate::gradient::intence::linear::LinearIntence;


fn make_constructor(method: &str) -> Box<Fn(Vec<(f64, f64)>) -> Box<dyn GradientIntence>> {
    match method {
        "bezier" => Box::new(|args|{Box::new(BezierIntence::new(args))}),
        "linear" => Box::new(|args|{Box::new(LinearIntence::new(args))}),
        _ => panic!("invalid intence method {}", method),
    }
}


pub struct IntenceFactory {
    f_params_begin: Vec<(f64, f64)>,
    f_params_end: Vec<(f64, f64)>,
    constructor: Box<Fn(Vec<(f64, f64)>) -> Box<dyn GradientIntence>>
}


impl IntenceFactory {

    pub fn new(
        method: &str,
        params_begin: Vec<(f64, f64)>,
        params_end: Vec<(f64, f64)>,
    ) -> IntenceFactory {
        assert_eq!(params_begin.len(), params_end.len());
        IntenceFactory {
            f_params_begin: params_begin,
            f_params_end: params_end,
            constructor: make_constructor(method),
        }
    }

    pub fn produce(&self, percent: f64) -> Box<dyn GradientIntence> {
        let mut params = vec![];
        for i in 0 .. self.f_params_begin.len() {
            let (begin_x, begin_y) = self.f_params_begin[i];
            let (end_x, end_y) = self.f_params_end[i];
            let x = begin_x + (end_x - begin_x) * percent;
            let y = begin_y + (end_y - begin_y) * percent;
            params.push((x, y));
        }
        (self.constructor)(params)
    }
}