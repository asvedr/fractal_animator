use crate::gradient::gradients::GradientIntence;


#[derive(Debug)]
struct CachedPoint {
    x: f64,
    y: f64,
    next_x: f64,
    to_next_x: f64,
    vec_y: f64,
}


pub struct LinearIntence {
    points: Vec<CachedPoint>,
    max_x: f64,
}


impl CachedPoint {
    fn new(x: f64, y: f64, next_x: f64, next_y: f64) -> CachedPoint {
        CachedPoint {
            x: x,
            y: y,
            next_x: next_x,
            to_next_x: next_x - x,
            vec_y: next_y - y
        }
    }
}


impl LinearIntence {
    pub fn new(points: Vec<(f64, f64)>) -> LinearIntence {
        let mut cached_points = vec![];
        for i in 0 .. points.len() - 1 {
            let (x, y) = points[i];
            let (nx, ny) = points[i+1];
            cached_points.push(CachedPoint::new(x, y, nx, ny));
        }
        let (x, y) = points[points.len() - 1];
        cached_points.push(CachedPoint::new(x, y, x, y));
        LinearIntence {
            points: cached_points,
            max_x: x,
        }
    }
}


impl GradientIntence for LinearIntence {
    fn get_intence(&mut self, value: f64) -> f64 {
        let scaled = value * self.max_x;
        for point in self.points.iter() {
            if scaled >= point.x && scaled <= point.next_x {
                let mul = (scaled - point.x) / point.to_next_x;
                return point.y + (point.vec_y * mul);
            }
        }
        panic!("Can't calculate value for {:?} in {:?}", scaled, self.points);
    }
}


#[cfg(test)]
mod tests {

    use super::*;
    use crate::gradient::gradients::GradientIntence;
    use crate::test_common;

    #[test]
    fn test_linear_intence_01() {
        let mut intence = LinearIntence::new(vec![(0.0, 0.0), (1.0, 1.0)]);
        assert_eq_float!(intence.get_intence(0.0), 0.0);
        assert_eq_float!(intence.get_intence(0.5), 0.5);
        assert_eq_float!(intence.get_intence(0.75), 0.75);
        assert_eq_float!(intence.get_intence(1.0), 1.0);
        let mut intence = LinearIntence::new(vec![(0.0, 0.0), (5.0, 1.0)]);
        assert_eq_float!(intence.get_intence(0.0), 0.0);
        assert_eq_float!(intence.get_intence(0.5), 0.5);
        assert_eq_float!(intence.get_intence(0.75), 0.75);
        assert_eq_float!(intence.get_intence(1.0), 1.0);
    }

    #[test]
    fn test_linear_intence_010() {
        let mut intence = LinearIntence::new(vec![(0.0, 0.0), (0.5, 1.0), (1.0, 0.0)]);
        assert_eq_float!(intence.get_intence(0.0), 0.0);
        assert_eq_float!(intence.get_intence(0.5), 1.0);
        assert_eq_float!(intence.get_intence(0.75), 0.5);
        assert_eq_float!(intence.get_intence(1.0), 0.0);
    }

    #[test]
    fn test_linear_intence_010_shifted() {
        let mut intence = LinearIntence::new(vec![(0.0, 0.0), (0.25, 1.0), (1.0, 0.0)]);
        assert_eq_float!(intence.get_intence(0.0), 0.0);
        assert_eq_float!(intence.get_intence(0.25), 1.0);
        assert_eq_float!(intence.get_intence(0.75), 0.33333);
        assert_eq_float!(intence.get_intence(1.0), 0.0);
    }
}
