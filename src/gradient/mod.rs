mod gradients;
pub mod intence;
pub mod color;

pub use gradients::{GradientIntence, GradientColor};
pub use color::ColorFactory;
pub use intence::IntenceFactory;
